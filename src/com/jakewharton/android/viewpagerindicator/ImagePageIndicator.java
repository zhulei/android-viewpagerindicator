/*
 * Copyright (C) 2011 Patrik Akerfeldt
 * Copyright (C) 2011 Jake Wharton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jakewharton.android.viewpagerindicator;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Draws circles (one for each view). The current view position is filled and
 * others are only stroked.
 */
public class ImagePageIndicator extends View implements PageIndicator {

    private ViewPager mViewPager;

    private ViewPager.OnPageChangeListener mListener;

    private int mCurrentPage;

    private int mCurrentOffset;

    private int mPageWidth;

    private Drawable normalDrawable;

    private float normalDrawableWidth;

    private float normalDrawableHeight;

    private Drawable checkedDrawable;

    private float checkedDrawableWidth;

    private float checkedDrawableHeight;

    public ImagePageIndicator(Context context) {
        this(context, null);
    }

    public ImagePageIndicator(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.imagePageIndicatorStyle);
    }

    public ImagePageIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ImagePageIndicator,
                defStyle, R.style.Widget_ImagePageIndicator);
        normalDrawable = a.getDrawable(R.styleable.ImagePageIndicator_normalSrc);

        normalDrawableWidth = normalDrawable.getIntrinsicWidth();
        normalDrawableHeight = normalDrawable.getIntrinsicHeight();

        checkedDrawable = a.getDrawable(R.styleable.ImagePageIndicator_checkedSrc);
        checkedDrawableWidth = checkedDrawable.getIntrinsicWidth();
        checkedDrawableHeight = checkedDrawable.getIntrinsicHeight();

        a.recycle();
    }

    /*
     * (non-Javadoc)
     * @see android.view.View#onDraw(android.graphics.Canvas)
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        final int count = mViewPager.getAdapter().getCount();
        final int paddingTop = getPaddingTop();
        final int paddingLeft = getPaddingLeft();
        final int paddingRight = getPaddingRight();
        final int paddingBottom = getPaddingBottom();

        float leftOffset = paddingLeft
                + (getWidth() - paddingLeft - paddingRight - count * normalDrawableWidth) / 2;
        float topOffset = paddingTop
                + (getHeight() - paddingTop - paddingBottom - normalDrawableHeight) / 2;
        for (int iLoop = 0; iLoop < count; iLoop++) {
            int left = (int)(leftOffset + iLoop * normalDrawableWidth);
            int top = (int)topOffset;
            int right = (int)(left + normalDrawableWidth);
            int bottom = (int)(top + normalDrawableHeight);
            normalDrawable.setBounds(left, top, right, bottom);
            normalDrawable.draw(canvas);
        }
        float cx = mCurrentPage * normalDrawableWidth;
        if (mPageWidth != 0) {
            cx += (mCurrentOffset * 1.0f / mPageWidth) * checkedDrawableWidth;
        }
        checkedDrawable.setBounds((int)(leftOffset + cx), (int)topOffset,
                (int)(leftOffset + cx + checkedDrawableWidth),
                (int)(topOffset + checkedDrawableHeight));
        checkedDrawable.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            final int count = mViewPager.getAdapter().getCount();
            final float halfWidth = getWidth() / 2;
            final float halfCircleWidth = (count * 3 * normalDrawableWidth / 2) / 2;

            if ((mCurrentPage > 0) && (event.getX() < halfWidth - halfCircleWidth)) {
                setCurrentItem(mCurrentPage - 1);
                return true;
            } else if ((mCurrentPage < count - 1) && (event.getX() > halfWidth + halfCircleWidth)) {
                setCurrentItem(mCurrentPage + 1);
                return true;
            }
        }

        return super.onTouchEvent(event);
    }

    @Override
    public void setViewPager(ViewPager view) {
        if (view.getAdapter() == null) {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
        mViewPager = view;
        mViewPager.setOnPageChangeListener(this);
        mPageWidth = mViewPager.getWidth();
        invalidate();
    }

    @Override
    public void setViewPager(ViewPager view, int initialPosition) {
        setViewPager(view);
        setCurrentItem(initialPosition);
    }

    @Override
    public void setCurrentItem(int item) {
        if (mViewPager == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        mViewPager.setCurrentItem(item);
        mCurrentPage = item;
        invalidate();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (mListener != null) {
            mListener.onPageScrollStateChanged(state);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        mCurrentPage = position;
        mCurrentOffset = positionOffsetPixels;
        mPageWidth = mViewPager.getWidth();
        invalidate();

        if (mListener != null) {
            mListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }
    }

    @Override
    public void onPageSelected(int position) {
        mCurrentPage = position;
        invalidate();

        if (mListener != null) {
            mListener.onPageSelected(position);
        }
    }

    @Override
    public void setOnPageChangeListener(ViewPager.OnPageChangeListener listener) {
        mListener = listener;
    }

    /*
     * (non-Javadoc)
     * @see android.view.View#onMeasure(int, int)
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    /**
     * Determines the width of this view
     * 
     * @param measureSpec A measureSpec packed into an int
     * @return The width of the view, honoring constraints from measureSpec
     */
    private int measureWidth(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        } else {
            // Calculate the width according the views count
            final int count = mViewPager.getAdapter().getCount();
            result = (int)(getPaddingLeft() + getPaddingRight() + (count * normalDrawableWidth)
                    + (count - 1) * normalDrawableWidth / 2 + 1);
            // Respect AT_MOST value if that was what is called for by
            // measureSpec
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    /**
     * Determines the height of this view
     * 
     * @param measureSpec A measureSpec packed into an int
     * @return The height of the view, honoring constraints from measureSpec
     */
    private int measureHeight(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        // We were told how big to be
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        }
        // Measure the height
        else {
            result = (int)(normalDrawableWidth + getPaddingTop() + getPaddingBottom() + 1);
            // Respect AT_MOST value if that was what is called for by
            // measureSpec
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState)state;
        super.onRestoreInstanceState(savedState.getSuperState());
        mCurrentPage = savedState.currentPage;
        requestLayout();
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.currentPage = mCurrentPage;
        return savedState;
    }

    static class SavedState extends BaseSavedState {
        int currentPage;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            currentPage = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(currentPage);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
